const User = require('../models/user');
const Doctor = require('../models/doctor');

authCtrl = { };

authCtrl.login = async (req, res) => {
  let user = await User.findOne({email: req.body.email, password: req.body.password});
  if (user) {
    req.session.user = user;
    let doctor = await Doctor.findOne({email: user.email});
    res.json(doctor);
  }
}

authCtrl.verify = async (req, res) => {
  if (!req.session.user) {
    res.status(404).json({
      message: 'Session not found'
    });
  } else {
    let doctor = await Doctor.findOne({email: req.session.user.email});
    res.json(doctor);
  }
}

module.exports = authCtrl;