const Appointment = require('../models/appointment');

const apptCtrl = { };

apptCtrl.createAppt = async (req, res) => {
  const appointment = new Appointment(req.body);
  await appointment.save();
  res.json(appointment);
}

apptCtrl.getApptsByDoctor = async (req, res) => {
  const { id } = req.params;
  if (id === undefined) {
    res.end();
  } else {
    const apptm = await Appointment.find({doctor: id})
    .populate('doctor', 'name email');
    res.json(apptm);
  }
}

apptCtrl.updateAppt = async (req, res) => {
  const { id } = req.params;
  const body = req.body;
  const appointment = {
    doctor: body.doctor,
    patient: body.patient,
    patientEmail: body.patientEmail,
    patientPhone: body.patientPhone,
    start: body.start,
    time: body.time,
    title: body.title,
    color: body.color,
    confirmed: body.confirmed
  };
  await Appointment.findByIdAndUpdate(id, {$set: appointment}, {new: true});
  res.json(true);
}

module.exports = apptCtrl;