const Doctor = require('../models/doctor');
const User = require('../models/user');

const drCtrl = { };

drCtrl.getDoctors = async (req, res) => {
  let doctors = await Doctor.find();
  res.json(doctors);
}

drCtrl.createDoctor = async (req, res) => {
  const user = new User(req.body);
  const doctor = new Doctor(req.body);
  if (await user.save()) {
    await doctor.save();
  }
  res.status(201).json(doctor);
}

drCtrl.getDoctor = async (req, res) => {
  const { id } = req.params;
  const doctor = await Doctor.findById(id);
  res.json(doctor);
}

module.exports = drCtrl;