const mongoose = require('mongoose');
const { Schema } = mongoose;

const appointmentSchema = new Schema({
  doctor: {type: Schema.Types.ObjectId, ref: 'doctor', required: true},
  patient: {type: String, required: true},
  patientEmail: {type: String, required: true},
  patientPhone: {type: String, required: true},
  start: {type: Date, required: true},
  time: {type: String},
  title: {type: String},
  color: {type: Object},
  confirmed: {type: Boolean}
});

module.exports = mongoose.model('appointment', appointmentSchema);