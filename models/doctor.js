const mongoose = require('mongoose');
const { Schema } = mongoose;

const doctorSchema = new Schema({
  name: {type: String, required: true},
  email: {type: String, required: true},
  phone: {type: String, required: true},
  speciality: {type: String},
  photo: {type: String}
});

module.exports = mongoose.model('doctor', doctorSchema);