const express = require('express');
const cors = require('cors');
const db = require('./database');
const multer = require('multer');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const app = express();
require('dotenv').config()
const path = require('path');

// Settings
app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, 'public')));

// Middlewares

app.use(session({
  secret: 'irbgui3h3481823!"$f',
  resave: true,
  saveUninitialized: false,
  cookie: {
    secure: false,
    maxAge: 3 * 24 * 60 * 60 * 1000
  },
  store: new MongoStore({ mongooseConnection: db,
  ttl: 14 * 24 * 60 * 60 // = 14 days. Default 
  })
}));

app.use(cors());
app.use(multer({dest: path.join(__dirname, 'public/upload/temp')}).single('image'))

app.options('*', cors());
app.use(express.json());

app.use('/v1/api/doctors', require('./routes/doctors.route'));
app.use('/v1/api/mailer', require('./routes/mailer.route'));
app.use('/v1/api/appointments', require('./routes/appointments.route'));
app.use('/v1/api/auth', require('./routes/auth.route'));

// Start server
app.get('*', (req, res, next) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
})

app.listen(app.get('port'), (req, res) => {
  console.log('server on port ', app.get('port'));
});
