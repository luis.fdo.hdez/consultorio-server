const express = require('express');
const router = express.Router();
const doctor = require('../controllers/doctors.controller');

router.get('/', doctor.getDoctors);
router.get('/:id', doctor.getDoctor);
router.post('/new', doctor.createDoctor);

module.exports = router;