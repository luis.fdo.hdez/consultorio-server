const express = require('express');
const router = express.Router();
const mail = require('../controllers/mailer.controller');

router.post('/confirm', mail.confirmApptm);

module.exports = router;