const express = require('express');
const router = express.Router();
const appointment = require('../controllers/appointments.controller');

router.post('/new', appointment.createAppt);
router.get('/:id', appointment.getApptsByDoctor);
router.put('/:id', appointment.updateAppt);

module.exports = router;